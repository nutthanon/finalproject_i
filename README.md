 # README #
This README would normally document to share information about this project.

### THIS PROJECT IS WORK-IN-PROGRESS (WIP) CONTAIN LATEST VERSION OF UPDATE IN GAME DEVELOP FLOW ###

* This project is a Final Project an part of subject GI242
* Using for Final Project presentation of subject GI242
 
 ### What is this repository for? ###

* For management files and control version of project and make backup.
* For keeping this repository as portfolio for group members
* [University BU](https://www.bu.ac.th/th/it-innovation/games-and-interactive-media)
 
 ### How do I get set up? ###

* Unity Engine
* Packages : Input System
* Packages : 2D Sprite (v1.0.0 March 03, 2021) *for edit image sprite

### Goal of this project ###

* Game flow > Start > Load Scene > Menu > Show summary
* Game Levels more than 1 levels
* Using Code convention, Structure, No Error at Run Time
* Game UI
 
### Owner of this project ###
 
* [1620704070](nutthanon.pats@bumail.net)
	-- Mr. Nutthanon Patsuwan
	-- Section 2522 No. 17

=========================================    :cherry_blossom:    =========================================