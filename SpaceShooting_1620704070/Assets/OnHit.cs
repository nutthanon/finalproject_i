﻿using UnityEngine;

public class OnHit : MonoBehaviour
{
    private float destroyTime = .3f;
    private void Update()
    {
        if (destroyTime > 0)
        {
            destroyTime -= Time.deltaTime;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
