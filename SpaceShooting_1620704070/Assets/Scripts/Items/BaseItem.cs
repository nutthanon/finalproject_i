﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Items
{
    public abstract class BaseItem : MonoBehaviour
    {

        public enum Type
        {
            Objective,
            Heal,
            Energy
        }
        
        public string Name { get; protected set; }
        public float Weight { get; protected set; }
        public float Cost { get; protected set; }
        public Type ItemType { get; protected set; }
        

        [SerializeField] private Rigidbody2D _rigidbody2D;
        private float speed = 1;

        public abstract void Init(string name, float weight, float cost, Type type);

        private void Update()
        {
            Move();
        }

        private void Move()
        {
            _rigidbody2D.velocity = Vector3.down * speed;
        }
        
    }
}