﻿using System;

namespace Items
{
    public interface ICollectable
    {
        event Action OnCollected;
        void Collected();
    }
}