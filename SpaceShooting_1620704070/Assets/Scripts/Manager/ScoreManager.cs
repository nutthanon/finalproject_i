﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        public int Score { get; protected set; }
        public int HighestScore { get; protected set; }


        private void Awake()
        {
            DontDestroyOnLoad(this);
        }
        
        public void Init()
        {
            Manager.Instance.GameManager.OnRestartGame += OnRestarted;
            SetScore(0);
        }

        public void SetScore(int score)
        {
            Score = score;
            Manager.Instance.UiManager.UpdateScoreText();
        }

        public void AddScore(int value)
        {
            Score += value;
            SetScore(Score);
        }
        
        public void SubScore(int value)
        {
            Score -= value;
            SetScore(Score);
        }

        public void SetHighestScore(int score)
        {
            HighestScore = score;
        }

        private void OnRestarted()
        {
            Manager.Instance.GameManager.OnRestartGame -= OnRestarted;
            SetScore(0);
        }
    }
}

