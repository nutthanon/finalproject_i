﻿using UnityEngine;

namespace Manager
{
    public class Manager : MonoBehaviour
    {
        //[Header("Manager")]
        public GameManager GameManager;
        public UIManager UiManager;
        public ScoreManager ScoreManager;
        public SoundManager SoundManager;
        
        public static Manager Instance {private set; get;} // Instance

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
            
            DontDestroyOnLoad(this);
        }
    }
}