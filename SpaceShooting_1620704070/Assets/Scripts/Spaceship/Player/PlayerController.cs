﻿using System;
using Items;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Spaceship.Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private SpriteRenderer spriteRenderer;

        private Vector2 movementInput = Vector2.zero;
        private ShipInputActions inputActions;
        private float minX;
        private float maxX;
        private float minY;
        private float maxY;

        private float currentReloadTime;
        private bool isReloadable;

        private void Awake()
        {
            InitInput();
            CreateMovementBoundary();

            currentReloadTime = 0;
        }

        private void InitInput()
        {
            inputActions = new ShipInputActions();
            inputActions.Player.Movement.performed += OnMove;
            inputActions.Player.Movement.canceled += OnMove;
            inputActions.Player.Fire.performed += OnFire;
            inputActions.Player.Fire.canceled += OnFire;
        }

        private void OnFire(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                if (playerSpaceship.Energy > 0)
                {
                    playerSpaceship.Fire();
                    isReloadable = false;
                }
                else
                {
                    isReloadable = true;
                }
            }

            if (context.canceled)
            {
                isReloadable = true;
            }
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                movementInput = context.ReadValue<Vector2>();
            }

            if (context.canceled)
            {
                movementInput = Vector2.zero;
            }
        }

        private void Update()
        {
            Move();

            if (isReloadable)
            {
                if (currentReloadTime < playerSpaceship.ReloadTime)
                {
                    if (currentReloadTime >= playerSpaceship.ReloadTime)
                    {
                        playerSpaceship.AddEnergy(1);
                    }
                    
                    currentReloadTime += Time.deltaTime;
                }
                
            }
        }

        private void Move()
        {
            var inputVelocity = movementInput * playerSpaceship.Speed;
            var newPosition = transform.position;
            newPosition.x = transform.position.x + inputVelocity.x * Time.smoothDeltaTime;
            newPosition.y = transform.position.y + inputVelocity.y * Time.smoothDeltaTime;
            
            // Clamp movement within boundary
            newPosition.x = Mathf.Clamp(newPosition.x, minX, maxX);
            newPosition.y = Mathf.Clamp(newPosition.y, minY, maxY);

            transform.position = newPosition;
        }

        private void CreateMovementBoundary()
        {
            var mainCamera = Camera.main;
            Debug.Assert(mainCamera != null,"mainCamera cannot be null");

            //var spriteRenderer = playerSpaceship.GetComponent<SpriteRenderer>();
            Debug.Assert(spriteRenderer != null, "spriteRenderer can not be null");

            var offset = spriteRenderer.bounds.size;
            minX = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).x + offset.x / 2;
            maxX = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).x - offset.x / 2;
            minY = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).y + offset.y / 2;
            maxY = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).y - offset.y / 2;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag($"Item/Heal"))
            {
                if (Manager.Manager.Instance.GameManager.Players.Count > 0)
                {
                    var player = Manager.Manager.Instance.GameManager.Players[0];
                
                    if (player.Health >= player.MaxHealth)
                    {
                        return;
                    }

                    var target = other.gameObject.GetComponent<ICollectable>();
                    target?.Collected();
                
                    Destroy(other.gameObject);
                }

            }
            
            if (other.CompareTag($"Item/Energy"))
            {
                if (Manager.Manager.Instance.GameManager.Players.Count > 0)
                {
                    var player = Manager.Manager.Instance.GameManager.Players[0];
                
                    if (player.Energy >= player.MaxEnergy)
                    {
                        return;
                    }

                    var target = other.gameObject.GetComponent<ICollectable>();
                    target?.Collected();
                
                    Destroy(other.gameObject);
                }
            }
        }

        private void OnEnable()
        {
            inputActions.Enable();
        }

        private void OnDisable()
        {
            inputActions.Disable();
        }
    }
}