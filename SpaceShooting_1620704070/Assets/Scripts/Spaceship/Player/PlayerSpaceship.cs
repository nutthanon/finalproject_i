﻿using System;
using UnityEngine;

namespace Spaceship.Player
{
    public class PlayerSpaceship : BaseSpaceship, IDamagable
    {
        public event Action OnExploded;
        public event Action OnFire;
        public event Action OnTakeHit;

        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            
        }

        public void Init(float maxHp, float maxEnegy, float speed, float reloadTime)
        {
            base.Init(maxHp, maxEnegy, speed, reloadTime, defaultBullet);
        }

        public override void Fire()
        {
            // 0 center, 1 left, 2 right
            SubEnergy(1);
            OnFire?.Invoke();
            var bulletCenter = Instantiate(defaultBullet, gunPosition[0].position, Quaternion.identity);
            bulletCenter.Init(Vector2.up);
        }

        public void TakeHit(int damage)
        {
            Health -= damage;
            OnTakeHit?.Invoke();
            if (Health > 0)
            {
                return;
            }
            Explode();
        }
        
        public void Explode()
        {
            Debug.Assert(Health <= 0,"Hp is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}