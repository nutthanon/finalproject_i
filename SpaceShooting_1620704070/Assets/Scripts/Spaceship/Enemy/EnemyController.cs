﻿using UnityEngine;
using Random = UnityEngine.Random;

namespace Spaceship.Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private Transform player;
        [SerializeField] private EnemySpaceship enemySpaceship;
        //[SerializeField] private float chasingThresholdDistance = 4.0f;
        [SerializeField] private SpriteRenderer spriteRenderer;

        private float enemySpeed = 4.0f;
        private float moveRate = 2.0f;
        private float moveCounter = 2.0f;
        private float fireRate = 2.0f; // 1 shot / 5 sec
        private float fireCounter; // 5 sec
        
        private float minX;
        private float maxX;
        private float minY;
        private float maxY;
        
        private void Awake()
        {
            CreateMovementBoundary();
        }

        private void Update()
        {
            if (moveCounter >= moveRate)
            {
                MoveToPlayer();
                moveCounter = 0;
            }

            if (fireCounter >= fireRate)
            {
                //Debug.Log("Shoot");
                enemySpaceship.Fire();
                fireCounter = 0;
            }
            
            moveCounter += Time.deltaTime;
            fireCounter += Time.deltaTime;
            
        }

        private void MoveToPlayer()
        {
            
            var playerPos = player.position;
            var enemyPos = transform.position;
            var originPosition = new Vector3(0, 3, 0);

            transform.position = Vector3.Lerp(enemyPos, originPosition, enemySpeed * Time.deltaTime);
            
            //var displacementToPlayer = playerPos - enemyPos;
            //var directionToPlayer = displacementToPlayer.normalized;
            //var distanceToPlayer = displacementToPlayer.magnitude;
            //var enemyVelocity = directionToPlayer * enemySpeed;
            
            var newTargetPos = new Vector3(0, 3, 0);;
            newTargetPos = new Vector3(Random.Range(-20,20), enemyPos.y, playerPos.z);

            transform.position = Vector3.Lerp(enemyPos, newTargetPos, enemySpeed * Time.deltaTime);
        }
        
        private void CreateMovementBoundary()
        {
            var mainCamera = Camera.main;
            Debug.Assert(mainCamera != null,"mainCamera cannot be null");

            //var spriteRenderer = playerSpaceship.GetComponent<SpriteRenderer>();
            Debug.Assert(spriteRenderer != null, "spriteRenderer can not be null");

            var offset = spriteRenderer.bounds.size;
            minX = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).x + offset.x / 2;
            maxX = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).x - offset.x / 2;
            minY = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).y + offset.y / 2;
            maxY = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).y - offset.y / 2;
        }
    }
}