﻿using System;
using UnityEngine;

namespace Spaceship.Enemy
{
    public class EnemySpaceship : BaseSpaceship, IDamagable
    {
        public event Action OnExploded;
        public event Action OnFire;
        public event Action OnTakeHit;

        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "Enemy Bullet cannot be null");
            Debug.Assert(gunPosition != null, "Enemy gunPosition cannot be null");
        }
        
        public void Init(float maxHp,float maxEnegy, float speed)
        {
            base.Init(maxHp,maxEnegy, speed,3, defaultBullet);
            Debug.Log($"Init Enemy by : MaxHP({maxHp}, MaxEG({maxEnegy})");
        }
        
        
        public override void Fire()
        {
            OnFire?.Invoke();
            var bulletCenter = Instantiate(defaultBullet, gunPosition[0].position, Quaternion.identity);
            bulletCenter.Init(Vector2.down);   
        }

        public void TakeHit(int damage)
        {
            Health -= damage;
            OnTakeHit?.Invoke();
            if (Health > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Health <= 0,"Hp is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}