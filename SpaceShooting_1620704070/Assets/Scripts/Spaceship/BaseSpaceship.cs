﻿using UnityEngine;

namespace Spaceship
{
    public abstract class BaseSpaceship : MonoBehaviour
    {
        [SerializeField] protected BaseBullet defaultBullet;
        [SerializeField] protected Transform[] gunPosition;

        public float MaxHealth { get; protected set; }
        public float Health { get; protected set; }
        public float MaxEnergy { get; protected set; }
        public float Energy { get; protected set; }
        public float Speed { get; private set; }
        public float ReloadTime { get; private set; }
        public BaseBullet Bullet { get; private set; }

        protected void Init(float maxHp,float maxEnergy, float speed,float reloadTime, BaseBullet bullet)
        {
            MaxHealth = maxHp;
            MaxEnergy = maxEnergy;
            Speed = speed;
            ReloadTime = reloadTime;
            Bullet = bullet;

            AddHealth(maxHp);
            AddEnergy(maxEnergy);
        }

        public void SetHealth(float value)
        {
            Health = value;
        }
        
        public void AddHealth(float value)
        {
            if (Health + value >= MaxHealth)
            {
                SetHealth(MaxHealth);
            }
            else
            {
                Health += value;
            }
            
            Manager.Manager.Instance.UiManager.UpdateHpFill();
        }
        
        public void SetEnergy(float value)
        {
            Energy = value;
        }

        public void AddEnergy(float value)
        {
            if (Energy + value >= MaxEnergy)
            {
                SetEnergy(MaxEnergy);
            }
            else
            {
                Energy += value;
            }
            
            Manager.Manager.Instance.UiManager.UpdateEnergyFill();
        }

        public void SubEnergy(float value)
        {
            if (Energy < 0)
            {
                Energy = 0;
            }

            else
            {
                Energy -= value;
            }
            
            Manager.Manager.Instance.UiManager.UpdateEnergyFill();
        }
        
        public void SetSpeed(float value)
        {
            Speed = value;
        }

        public abstract void Fire();
    }
}


