﻿using System;
using UnityEngine;

namespace Spaceship
{
    public class BaseBullet : MonoBehaviour
    {
        [SerializeField] private int damage;
        [SerializeField] private float speed;
        [SerializeField] private Rigidbody2D _rigidbody2D;
        [SerializeField] private GameObject hitEffect; 
        [SerializeField] private float destroyTime = 5;
        
        public event Action OnBulletHit;
        public event Action OnBulletSpawn;
        
        public void Init(Vector2 bulletDirection)
        {
            Move(bulletDirection);
        }

        private void Awake()
        {
            Debug.Assert(_rigidbody2D != null, "rigidbody2D cannot be null");
            OnBulletSpawn?.Invoke();
        }

        private void Update()
        {
            if (destroyTime > 0)
            {
                destroyTime -= Time.deltaTime;
            }
            else
            {
                Destroy(this.gameObject);
            }
        }

        public void Move(Vector2 direction)
        {
            _rigidbody2D.velocity = direction * speed;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            OnBulletHit?.Invoke();
            Instantiate(hitEffect,this.transform.position,Quaternion.identity);
            var target = other.gameObject.GetComponent<IDamagable>();
            target?.TakeHit(damage);
            Destroy(this.gameObject);
        }
    }
}

